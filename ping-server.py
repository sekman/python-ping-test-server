import BaseHTTPServer, SimpleHTTPServer
import ssl
import sys

port = 8443
if len(sys.argv[1]) < 1:
    print "Using default port 8443"
else:
    port = int(sys.argv[1])


class S(BaseHTTPServer.BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.protocol_version = "HTTP/1.1"
        self.send_header('Content-Length', "4")
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        self.wfile.write("true")

S.protocol_version = "HTTP/1.1"
httpd = BaseHTTPServer.HTTPServer(('0.0.0.0', port), S)
httpd.socket = ssl.wrap_socket (httpd.socket, certfile='./server.pem', server_side=True)
httpd.serve_forever()
